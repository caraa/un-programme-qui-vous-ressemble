# Créé par rousseleth, le 02/02/2024 en Python 3.7
def je_veux_un_super_programme_sportif():

    programme1 = 0
    programme2 = 0
    programme3 = 0
    programme4 = 0
    programme5 = 0
    programme6 = 0
    programme7 = 0
    programme8 = 0

    prenom = input (" Comment vous appelez vous ? (Prénom) ")

    nom = input (" Comment vous appelez vous ? (Nom) ")

    taille = input ("Quelle est votre taille ? (cm) ")

    age = int(input (" Quel est votre Age ? "))
    if age <= 15 :
        programme1 = programme1 + 1
        programme2 = programme2 + 1


    elif age <= 25 and age > 15 :
        programme1 = programme1 + 1
        programme2 = programme2 + 1
        programme3 = programme3 + 1

    elif age <= 35 and age > 25 :
        programme1 = programme1 + 1
        programme2 = programme2 + 1
        programme3 = programme3 + 1
        programme4 = programme4 + 1
        programme5 = programme5 + 1


    elif age <= 50 and age > 35 :
        programme1 = programme1 + 1
        programme2 = programme2 + 1
        programme3 = programme3 + 1
        programme4 = programme4 + 1


    elif age > 50 :
        programme1 = programme1 + 1
        programme2 = programme2 + 1


    presentation = 0
    while presentation != 1 :
        presentation = int(input("Dans les questions qui vont suivre veuillez répondre avec des chiffres allant de 1 à 4 :    1/ J'ai compris  "))

    sexe = 5
    while sexe >= 4 :
        sexe = int(input("Quelle est votre sexe ?  1) Masculin   2) Feminin   3) Autre "))

    disponibilite = 5
    while disponibilite <= 0 or disponibilite >= 4:
        disponibilite = int(input(" Quand êtes-vous disponible ?    1) Le Week-End et la Semaine   2) Uniquement le Week-End     3) Uniquement la Semaine  "))
        if disponibilite == 1 :
            programme1 = programme1 + 1
            programme2 = programme2 + 1
            programme3 = programme3 + 1
            programme4 = programme4 + 1
            programme5 = programme5 + 1


        elif disponibilite == 2 :
                programme7 = programme7 + 8

        elif disponibilite == 3 :

            programme6 = programme6 + 8


    club_ou_pas = 4
    while club_ou_pas <= 0 or club_ou_pas >= 3 :
        club_ou_pas = int(input(" Pratiquez-vous un sport a haut niveau (Professionel) ?    1) Oui    2) Non "))

        if club_ou_pas == 1 :
            programme8 = programme8 + 10

    frequence_semaine = 6
    while frequence_semaine <= 0 or frequence_semaine >= 5 :
        frequence_semaine = int(input(" A quelle fréquence faites vous du sport par semaine ? 1) Moins de 1h   2) Moins de 2h    3) Plus que 3h   4) Plus de 5h "))

        if frequence_semaine == 1:
            programme1 = programme1 + 1

        elif frequence_semaine == 2:
            programme2 = programme2 + 1
            programme3 = programme3 + 1

        elif frequence_semaine == 3:

            programme4 = programme4 + 1
            programme3 = programme3 + 1
        else:

            programme4 = programme4 + 1
            programme5 = programme5 + 1


    heure = 5
    while heure <= 0 or heure >= 5 :
        heure = int(input(" Combien d'heure en moyenne par jour ?  1) Moins de 30 minutes    2) Environ 1h    3) Plus de 1h   "))

        if heure == 1:
            programme1 = programme1 + 1

        elif heure == 2:
            programme2 = programme2 + 1
            programme3 = programme3 + 1

        elif heure == 3:
            programme4 = programme4 + 1
            programme5 = programme5 + 1

    preference1 = 6
    while preference1 <= 0 or preference1 >= 5 :
        preference1 = int(input(" Pourquoi faites vous ou voudriez vous faire du sport ?    1) Prendre l'air  2) Se dépenser tranquillement  3) S'entretenir  4) Performer "))

        if preference1 == 1:
            programme1 = programme1 + 1
            programme2 = programme2 + 1

        elif preference1 == 2:
            programme2 = programme2 + 1
            programme3 = programme3 + 1

        elif preference1 == 3:
            programme4 = programme4 + 1
            programme3 = programme3 + 1

        else:
            programme5 = programme5 + 1

    preference2 = 6
    while preference2 <= 0 or preference2 >= 5 :
        preference2 = int(input("Lorsque vous faites du sport quelle est la chose que vous recherchez ?    1) Tranquilité  2) Joie  3) Un défoulement quotidient   4)Un dépassement de soit même "))

        if preference2 == 1:
            programme1 = programme1 + 1

        elif preference2 == 2:
            programme2 = programme2 + 1

        elif preference2 == 3:
            programme4 = programme4 + 1
            programme3 = programme3 + 1

        else:
            programme5 = programme5 + 1

    print()
    print('Une séance aérobie consiste à faire 1h ou plus d un sport qui vous convient (course à pied, marche, vélo...)')
    print()
    print('Une séance musculation consiste à faire 30 minutes ou plus d exercice de renforcement musculaire (Eric Flag, entrainements personnels à la salle, petits exercices maisons...)')
    print()
    print('Il est important de ne pas se fier uniquement à ce programme et de ne pas hésiter à faire des sports collectifs entre amis et de profiter de la vie. ')
    print()

    T = [programme1, programme2, programme3, programme4, programme5, programme6, programme7, programme8]

    print(T)
    maxi = 0
    for element in T:
        if element > maxi :
            maxi = element


    if maxi == T[0]:
        print('Le programme le plus adapté à vous est : Programme 1')
        print('Mr ou Mme', nom, prenom ,'je vous conseil de faire: Une séance aérobie le mercredi ou le samedi.')
        print()

    elif maxi == T[1]:
        print('Le programme le plus adapté à vous est : Programme 2')
        print('Mr ou Mme', nom, prenom ,'je vous conseil de faire: Une séance aérobie le mercredi et une séance musculation le samedi.')
        print()

    elif maxi == T[2]:
        print('Le programme le plus adapté à vous est : Programme 3')
        print('Mr ou Mme', nom, prenom ,'je vous conseil de faire: Deux séance aérobie le mardi et samedi et une séance musculation le jeudi.')
        print()

    elif maxi == T[3]:
        print('Le programme le plus adapté à vous est : Programme 4')
        print('Mr ou Mme', nom, prenom ,'je vous conseil de faire: Deux séance aérobie le lundi et vendredi et deux séance musculation le mercredi et samedi.')
        print()

    elif maxi == T[4]:
        print('Le programme le plus adapté à vous est : Programme 5')
        print('Mr ou Mme', nom, prenom ,'je vous conseil de faire: Trois séance aérobie le lundi, mercredi et vendredi et Trois séance musculation le mardi, jeudi et samedi.')
        print()

    elif maxi == T[5]:
        print('Le programme le plus adapté à vous est : Programme 6')
        print('Mr ou Mme', nom, prenom ,'je vous conseil de faire: Deux séance aérobie le lundi et vendredi et deux séance musculation le mercredi.')
        print()

    elif maxi == T[6]:
        print('Le programme le plus adapté à vous est : Programme 7')
        print('Mr ou Mme', nom, prenom ,'je vous conseil de faire: Une séance aérobie le samedi et une séance musculation le dimanche (si possible faire une séance aérobie ou muscu la semaine).')
        print()

    elif maxi == T[7]:
        print('Le programme le plus adapté à vous est : Programme 8')
        print('Mr ou Mme', nom, prenom ,'je vous conseil de faire: une séance aérobie et une séance musculation en plus des entrainements en club.')
        print()












